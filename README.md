# Simple ESP8266 Weather Station

## Description 

This is an example about how to use the MicroPython's `urequests` module. It retrieves weather data from [OpenWeatherMap Weather API](https://openweathermap.org/api) and displays it on a 20 x 4 HD44780U based LCD.

![alt](https://i.imgur.com/ZnSljIn.jpg)

## Useful links & Credits

- HD44780U-LCD driver class is based on <http://github.com/wjdp/micropython-lcd>
- List of all OpenWeatherMap API parameters with units [openweathermap.org/weather-data](https://openweathermap.org/weather-data)
- MicroPython's `urequests` module [micropython-lib/urequests](https://github.com/micropython/micropython-lib/tree/master/urequests)